.POSIX:

STOW := stow
TARGET := ${HOME}

PACKAGES := tmux vim bash bin

all: ${PACKAGES}
	$(STOW) -R $^

clean:
	$(STOW) -D ${PACKAGES}

.PHONY: clean all
