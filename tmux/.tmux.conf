# settings
set -g focus-events on
set -g base-index 1

set -g default-terminal   "$TERM"
set -g terminal-overrides "$TERM:Tc"

# prefix
unbind C-b
set -g prefix C-Space
bind C-Space send-prefix

# bindings
bind-key "|"  split-window -h  -c "#{pane_current_path}"
bind-key "\\" split-window -fh -c "#{pane_current_path}"
bind-key "-"  split-window -v  -c "#{pane_current_path}"
bind-key "_"  split-window -fv -c "#{pane_current_path}"
bind-key "%"  split-window -h  -c "#{pane_current_path}"
bind-key '"'  split-window -v  -c "#{pane_current_path}"

# emacs like movement
bind-key -T copy-mode-vi C-n send-keys -X cursor-down
bind-key -T copy-mode-vi C-p send-keys -X cursor-up

# Smart pane switching with awareness of Vim splits.
# See: https://github.com/christoomey/vim-tmux-navigator
is_vim="ps -o state= -o comm= -t '#{pane_tty}' \
    | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?(view|n?vim?x?)(diff)?$'"
    bind-key -n C-h if-shell "$is_vim" "send-keys C-h"  "select-pane -L"
    bind-key -n C-j if-shell "$is_vim" "send-keys C-j"  "select-pane -D"
    bind-key -n C-k if-shell "$is_vim" "send-keys C-k"  "select-pane -U"
    bind-key -n C-l if-shell "$is_vim" "send-keys C-l"  "select-pane -R"
    bind-key -n C-\ if-shell "$is_vim" "send-keys C-\\" "select-pane -l"
    bind-key -T copy-mode-vi C-h select-pane -L
    bind-key -T copy-mode-vi C-j select-pane -D
    bind-key -T copy-mode-vi C-k select-pane -U
    bind-key -T copy-mode-vi C-l select-pane -R
    bind-key -T copy-mode-vi C-\ select-pane -l

# clear screen
bind-key C-l send-keys C-l
